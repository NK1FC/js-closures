function limitFunctionCallCount(cb, n) {
    if(arguments.length < 2 || typeof cb !== 'function' || typeof n !== 'number'){
        throw new Error("Errors in arguments");
    }
    let count = 0;
    return function (...args) {

        if (count < n) {
            count++;
            return cb(...args);
        }
        return null;
    };
}


module.exports = limitFunctionCallCount;