const cacheFunction = require("../cacheFunction.cjs")

function sum(a,b){
    return a+b;
}


test('CacheFunction', ()=>{
    const a = cacheFunction(sum)
    expect(a(1,2)).toBe(3);
})