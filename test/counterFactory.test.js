const counterFactory = require("../counterFactory.cjs");

test("counterFactory" ,()=> {
    let value = counterFactory();
    expect(value.increment()).toBe(1);
    expect(value.increment()).toBe(2);
    expect(value.decrement()).toBe(1);
    expect(value.decrement()).toBe(0);
});
