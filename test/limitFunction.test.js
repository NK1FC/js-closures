const limitFunctionCallCount = require('../limitFunctionCallCount.cjs')


function sum(a,b){
    return a+b;
}


test('limitFunction', ()=>{
    const a = limitFunctionCallCount(sum,2)
    expect(a(1,2)).toBe(3);
    expect(a(1,2)).toBe(3);
    expect(a(1,2)).toBe(null);
})