function cacheFunction(cb) {
    if (typeof cb !== 'function' || arguments.length < 1) {
        throw new Error("Errors in arguments");
    }

    let cache = {};
    function storeAndReturn(...args) {
        if (cache.hasOwnProperty(args)) {
            return cache[args];
        } else {
            cache[args] = cb(...args);
            return cache[args];
        }
    }

    return storeAndReturn;
}



module.exports = cacheFunction;

